---
layout: markdown_page
title: "Personas Direction - Designers"
description: "Here you can find information on GitLab's Personas Direction - Designers. Learn more here!"
canonical_path: "/direction/designers/"
---

- TOC
{:toc}

## Who are designers?
TBD

## What's next & why
- [Versioned designs](https://gitlab.com/groups/gitlab-org/-/epics/660)

## Key Themes
- [Design reviews](https://gitlab.com/groups/gitlab-org/-/epics/990)
- [Design process automation](https://gitlab.com/groups/gitlab-org/-/epics/991)

## Stages with design focus

There are several stages involved in developing the executive's toolbox at GitLab. These include, but are not necessarily limited to the following:
- [Create](/direction/create/)
