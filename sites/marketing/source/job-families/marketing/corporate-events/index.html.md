---
layout: job_family_page
title: "Corporate Events"
---
 
### Corporate Events Manager
 
## Levels
 
### Corporate Events Manager (Intermediate)
 
The Corporate Events Manager (Intermediate) reports to Manager, Corporate Events and Branding.
 
#### Corporate Events Manager (Intermediate) Job Grade 
 
The Corporate Events Manager (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Corporate Events Manager (Intermediate) Responsibilities
 
* Oversee execution of corporate events and swag to amplify our brand story and image at national and international trade shows, internal events, user conferences, brand activations and all other events.
* Proactively manage and strategize all event needs. End-to-end event management, from lead-handling and content creation to venue selection and event execution.
* Establish event goals and budget: gather and track ROI, engagement analytics, and feedback to consistently assess and implement opportunities for improvement.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, alliances, demand generation and field teams.
* Advance, maintain, and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas, campaigns, and content that meet engagement targets.
* Oversee creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.
* Solicit speaking sessions for all relevant corporate conferences and industry trade shows.
 
#### Corporate Events Manager (Intermediate) Requirements
 
* 3+ years corporate marketing events planning and management experience in the high-tech industry.
* Self-sufficient worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Proven skills interacting with executive/senior management teams.
* Exemplary project management and decision-making skills.
* Very detail-oriented.
* Flexible work schedule and the availability to travel approximately 50% of the time.
* Ability to use GitLab
* **Additional Requirement for Europe candidates**:
 * Must be in Europe and must be fully eligible to travel within the EU.
 * Regional knowledge and existing network of vendors in EMEA.
 
### Senior Corporate Events Manager
 
The Senior Corporate Events Manager reports to Manager, Corporate Events and Branding.
 
#### Senior Corporate Events Manager Job Grade
 
The Senior Corporate Events Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Senior Corporate Events Manager Responsibilities
 
* Extends that of the Corporate Events Manager (Intermediate) responsibilities
 
#### Senior Corporate Events Manage Requirements
 
* Extends that of the Corporate Events Manager (Intermediate) Requirements
* 5+ years corporate marketing events planning and management experience in the high-tech industry.
 
## Specialties
 
### Sales Events
 
* Focus on Sales specific Events
* Flexible work schedule and the ability to travel approximately 30% of the time.
 
### Production Manager
 
The Production Manager reports to Manager, Corporate Events and Branding.
 
#### Production Manager Responsibilities
 
* Ensuring there is a DRI for all content-related tasks for corporate marketing events to see through a successful execution of all elements of the event. This includes making sure all elements of branded events have an owner and reporting back on updates and deadlines.
* Working closely with event DRI, GitLab digital production and Local to provide quality AV systems and on‐site support for live corporate events. Collaborates to build event orders for audio, video, staging, and lighting systems. Assist in the planning and coordination of productions, including but not limited to scouting locations, talent, and crew.
* Working hand-in-hand with digital production on how content is used and promoted post-event. Potential writing and/or editing treatments and scripts for multimedia packages.
* Liaising with internal teams to assist with pre- and post-production for GitLab-branded audio and video projects and events, including but not limited to Contribute, Sales Events, Commit user conferences, Developer Evangelism, Community, Employment branding, sales enablement, social, and livestreams.
* Writing keynote scripts and acting as the day-of contact for speakers and running the day-of show for all event production-related needs for opening and closing keynotes.
* Working with brand, design, and social media to make sure brand is represented in slide decks.
* Managing local production staff and accounts.
* Owning the integrated marketing campaigns and connecting the dots between demand generation, content marketing, and corporate events.
* Managing the corporate events branded website design and execution to drive ticket sales and properly promote our events overall.
* Manage our self-hosted events Call For Proposals (CFP) process, working hand-in-hand with internal and external thought leaders on their talk submissions.
* Review slide decks and develop a speaker training program.
 
#### Production Manager Requirements
 
* Excellent written and verbal communication skills
* Exceptional organizational skills
* Relationships in the software DevOps space are a plus
* Substantial knowledge with live corporate events and conferences.
* Highly collaborative, fostering effective relationships across all parts of the business and must enjoy learning/sharing knowledge
* This position will require doing occasional work on‐site and includes occasional weekend work
* Anticipated travel 15 to 20%
* 5+ years in events work and 2+ in software or tech related space
 
### Manager, Corporate Events
 
The Manager, Corporate Events reports to Senior Director, Corporate Marketing.
 
#### Manager, Corporate Events Job Grade
 
The Manager, Corporate Events is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Manager, Corporate Events Responsibilities
 
* Excel at all of the corporate events manager responsibilities above.
* Oversee strategic and creative development of corporate events and swag strategy to amplify our brand story and image at national and international trade shows and events.
* Strategically connect business priorities, goals, and key messages for complete brand experience.
* Develop and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Be a team player responsible to build out and manage processes that will ensure the success of our events across functional groups and with production partners.
* Create corporate event marketing swag vision and strategy that integrates brand, design, content, corporate events, and PR to increase awareness and engagement, and express brand personality with every interaction.
* Coordinate with design to develop swag strategy as part of brand persona. Evoke spirit of brand and personality in swag items.
* Perform all management tasks for managing an effective, results focused team.
* Participate with peers in creating and executing the Corporate Marketing strategy.
 
#### Manager, Corporate Events Requirements
 
* 5-7 years corporate events planning and management experience in the high-tech industry.
* Strategic marketing experience that goes beyond event operations/production, and includes substantial understanding of marketing communications, campaigns, event messaging, product and corporate content, and customer experience.
* Self-sufficient worker capable of managing multiple deadlines with little supervision.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Proven skills interacting with executive/senior management teams.
* Exemplary project management and decision-making skills.
* Very detail oriented.
* Flexible work schedule and the ability to travel approximately 30% of the time.
 
### Senior Manager, Brand Activation
 
The Senior Manager, Brand Activation reports to Director, Corporate Marketing.
 
#### Senior Manager, Brand Activation Responsibilities
 
* Oversee execution of corporate events, integrated brand campaigns, and swag to amplify our brand story and image at national and international trade shows, internal events, brand activations and all other events.
* Support other external brand channels such as social, Web, etc. to make sure the brand experience is consistent.
Manage corporate events and design team to bring together a cohesive brand strategy that can be executed across events, and help GitLab become an engaging and household recognizable name.
* Pilot cross-functional brand experience design programs to forge connections with our brand among internal employees, prospects, customers, partners, supported employees and more. Ensure all campaign touchpoints reflect and represent the GitLab brand and story.
* Provide strategy to the design team on how people will engage with our brand through different marketing channels. Develop experience and design strategy to provide direction on content and campaign development to align with organizational initiatives and goals.
* Proactively manage and strategize all event needs. End-to-end event management, from lead-handling and content creation to venue selection and event execution.
* Establish event goals and budget: gather and track ROI, engagement analytics, and feedback to consistently assess and implement opportunities for improvement.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, alliances, demand generation and field teams.
* Advance, maintain, and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas, campaigns, and content that meet engagement targets.
* Oversee creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.
 
#### Senior Manager, Brand Activation Requirements
 
* Familiarity with the GitLab brand and how the company has grown and developed to this day. Vision on how to take our experience with the brand to the next level.
* Capable of setting strategy for multiple teams that map back to an overarching theme.
* Exemplary communicator and collaborator; able to work with a variety of teams and personalities, possessing excellent verbal and written communications skills.
* Ability to work smart under pressure and efficiently on multiple project requests simultaneously, and to deal with potential for tight deadlines and unexpected complexities.
* Proven skills interacting with executive/senior management teams.
* Very detail-oriented.
* Flexible work schedule and the availability to travel approximately 50% of the time.
* Ability to use GitLab.

### Manager, Brand Growth

#### Manager, Brand Growth Job Grade 
 
The Brand Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Manager, Brand Growth Responsibilities

* Identify how our brand is currently positioned in the market and develop a reporting process to document the progress of our brand
* Implement brand measurement systems so the value of our brand can be easily tracked
* Oversee the brand message creation process, as well as make sure the copy on our web properties align with the message
* Develop and execute marketing campaigns aimed at communicating our brand message 
* Educate and communicate our brand personality internally and align company around the message 
* Develop brand guidelines and communication programs so entities outside of GitLab, can easily understand how to use our brand
* Brand management for international expansion, including production of brand guidelines in collaboration with the Brand team.
* Measure and report on success of brand marketing campaigns
* Oversee merchandise management 
* Provide support with brand team process establishment and documentation 
* Lead Brand Growth Content team 

#### Manager, Brand Growth Requirements

* Ability to use GitLab
* 5-7 years experience in a brand strategy role
* Knowledge of brand tracking and brand measurement systems
* Capability to coordinate across many teams and perform in fast-moving startup environment
* Understand and be able to express the value of GitLab as a brand entity
* Outstanding written and verbal communications skills
* You embrace our values, and work in accordance with those values.
 
## Performance Indicators
 
* Staying within or under budget on AV, staging, projects.
* Positive speaker and audience feedback on produced events (from survey results).
* Continued speaker engagement.
* Expansion of speaker and vendor catalogue.
 
## Career Ladder
 
The next step in the Corporate Events job family is not yet defined at GitLab.
 
## Hiring process
 
Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
 
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 teammates
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
