---
layout: job_family_page
title: "Channel Services"
---

## Senior Channel Services Manager

The Channel Services Manager will serve as a primary point of contact for Gitlab’s top services partners and will ensure that services partners are trained and certified to provide professional, consulting, managed  & advisory services for Gitlab customers. This individual will help Gitlab manage the evolution of its professional services offerings via partners to drive usage, adoption of the complete Gitlab platform and expansion into the customer.

### Job Grade

The Senior Channel Services Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Provide a channel of communication between the partner and Gitlab to ensure smooth and effective customer implementation processes, seamless Managed Services and value-driven strategic advisory services.
* Identify, define and assist in building services offerings and/or practices at the partner that support all levels of the Gitlab Customer Service cycle.
* Content and Tools Creation - Support the Gitlab Enablement, Professional Services & Customer Success teams in building and improving tools and processes that promote mutual success.
* Working with the Channel Sales Managers, drive and facilitate partner certifications through eLearning, instructor-led training, boot camps and ongoing certification maintenance.
* Find and recommend new Services Partners in collaboration with corporate and regional Channel Sales Managers
* Ensure our partners have the skills, training, and expertise to implement, operate  and support Gitlab solutions.

### Requirements

* Must demonstrate the capability to excel within a cross-functional team environment.
* Experience building channel services & practices in SaaS/subscription models.
* Strong presentation and written communications skills. Previous experience enabling partners to deliver services that grow revenue, expand customer footprint and drive renewals.
* Excellent strategic planning, project management, communication and presentation skills.
* 7-10 years of working experience in partner management, training, product marketing, professional services, or product management in the high-tech industry
* BS/BA required or equivalent experience; MBA a plus
* Experience in the DevOps space a plus
* Ability to use GitLab

## Channel Services Manager

### Job Grade

The Channel Services Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Identify, define and assist in building services offerings &/or practices at the partner that support all levels of the Gitlab Customer Service cycle - primarily focused on the GTM components or pricing, packaging, positioning, demand generation & revenue attribution.
* Provides input into pricing strategies for new products; as well as consults on pricing changes for existing products
* Identify, define and assist in building services offerings &/or practices at the partner that support all levels of the Gitlab Customer Service cycle - with specific focus
* Content and Tools Creation - Support the Gitlab Enablement, Professional Services & Customer Success teams in building and improving tools and processes that promote mutual success.
* Working with the Channel Sales Managers, drive and facilitate the launch of partner Gitlab practices, service & solution offerings.
* Find and recommend new Services Partners in collaboration with corporate and regional Channel Sales Managers
* Ensure our partners, and the channel as a whole,  have the alignment to Gitlab Marketing, PMM required  to implement, operate  and support Gitlab solutions.

#### Requirements

* Must demonstrate the capability to excel within a cross-functional team environment.
* Strong presentation and written communications skillsPrevious experience enabling partners to deliver services that grow revenue, expand customer footprint and drive renewals.
* Excellent strategic planning, project management, communication and presentation skills.
* 7-10 years of working experience in partner management, training, product marketing, professional services, or product management in the high-tech industry
* BS/BA required or equivalent experience; MBA a plus
* DevOps experience a plus
* Ability to use GitLab

## Specialties

Read more about what a [specialty](/handbook/hiring/vacancies/#definitions) is at GitLab.

### Go To Market (GTM)

### Performance Indicators
[Sales KPI's](/handbook/ceo/kpis/#sales-kpis)

## Career Ladder

The next step for a Channel Services Manager is to move into the [Channel Sales Manager](/job-families/sales/channel-sales-manager/) Job Family.

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
1. Phone screen with a GitLab Recruiting team member
2. Video Interview with the Hiring Manager
3. Team Interviews with 1-4 teammates
Additional details about our process can be found on our [hiring page](/handbook/hiring).
